//MAC ADDRESS  bcddc223f93e

// Librerie Per DHT //
#include <Arduino.h>
#include "DHTesp.h"

//Librerie Per Hurricane //
#include <FS.h>               //libreria SPIFFS dati registrazione

// Solo per MQTT-SSL e ESP8266//
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <time.h>
// Importante per WiFi Manager //
#include <WiFiManager.h>
#include <DNSServer.h>            //Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h>     //Local WebServer used to serve the configuration portal

// Librerie aggionamento SELFOTA WEB
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
// Definisco Fp per TLS
#define CHECK_FINGERPRINT

DHTesp dht;
char charBuf[130];

// Stringhe per dati
String humidity;
String temperature;

//---------------------------------------- Definisco Server per aggiornamento e release programma in esecuzione //
const char* fwUrlBase = "http://casabada.homepc.it/fota/";

//const char* fwUrlBase = "http://192.168.1.6/fota/";
const int LED =4;             //Assegna pin 19 (GPIO 4) a LED
const int LED1 =5;            //Assegna pin 20 (GPIO 5) a LED
const int FW_VERSION = 8;    //---------------------> numero di versione
const int RELE = 13;           //Assegna pin 7  (GPIO 13) a RELE'


// Definisco parametri rete WiFi //
#ifndef SECRET                  //commentare per connessione MQTTS


//Memorizzo la FingerPrint del certificato Cert.pem generato con let's cert - comando per estrarlo  "openssl x509 -fingerprint -in cert.pem" //
    #ifdef CHECK_FINGERPRINT
    //static const char fp[] PROGMEM = "AA:8B:BF:AA:C4:1F:55:13:EB:B2:43:DF:AE:40:43:C7:45:B0:2C:D5"; // 5 Zibawa Davide5
      static const char fp[] PROGMEM = "07:AA:2B:1B:C8:D9:66:F7:04:2E:F8:FE:8A:D0:16:69:3D:83:6C:38"; //hc.terzo.tech
  #endif

#endif

////////////l2VjbBJjBVbkvJT4cjJ4VYih//////////////////////////////////////////

//Definisco Classe MQTT --> 1 Mqtt , 2 Mqtts
//WiFiClient espClient;             //1
//PubSubClient client(espClient);   //1
BearSSL::WiFiClientSecure net;  //2
PubSubClient client(net);       //2

//Definisco qui valori Default, if there are different values in config.json, they are overwritten.
char mqtt_server[40] = "mqtt.terzo.tech";
char mqtt_port[6] = "8883";
char username[50] = "f5fc70ae-7d25-475e-a5f6-099fd32915c7";   // Impostare ID E PASSWORD
char password[34] = "Prova10022021";

const char* willTopic = "graveyard/";

//Definisco Nome visualizzato su Broker //
    #define HOSTNAME "Meter_casetta"                       

//Imposto Il top di sottoscrizione e pubblicazione //
    const char topic1[] = "upgrade";           //comando per riavviare ESP8266
    const char topic2[] = "rele";             //comando per Attivare Rele
    const char topicpub1[] = "v_firmware";    // pubblica versione FW
    const char topicpub2[] = "stato_rele";    // pubblica versione FW

//Imposto nome che prenderà in modalità AP
const char* apName = "Terzo-tech-company";
//flag for saving data
bool shouldSaveConfig = false;

//notifica di salvataggio configurazione
void saveConfigCallback () {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}
long lastMsg = 0;
char msg[100];
int value = 0;

unsigned long lastMillis = 0;

String getMAC()
{
  byte mac[6];
  char result[14];
  WiFi.macAddress(mac);

 snprintf( result, sizeof( result ), "%02x%02x%02x%02x%02x%02x", mac[ 0 ], mac[ 1 ], mac[ 2 ], mac[ 3 ], mac[ 4 ], mac[ 5 ] );

  return String( result );
}
//e' possibile aggiungere dei parametri extra
  WiFiManagerParameter custom_mqtt_server("server", "mqtt server", mqtt_server, 40);
  WiFiManagerParameter custom_mqtt_port("port", "mqtt port", mqtt_port, 5);
  WiFiManagerParameter custom_username("username", "username", username, 50);
  WiFiManagerParameter custom_password("password", "password", password, 32);


//---------------------------------------- Programma connessione WiFi Manager //
void setup_wifi() {
                    //Inizializzo WifiManager
                    WiFiManager wifiManager;
                    //imposto parametri Richiesta configurazione
                    wifiManager.addParameter(&custom_mqtt_server);
                    wifiManager.addParameter(&custom_mqtt_port);
                    wifiManager.addParameter(&custom_username);
                    wifiManager.addParameter(&custom_password);
                    //definisco timeout Captive Portal
                     wifiManager.setTimeout(120);

                    //wifiManager.setSTAStaticIPConfig(IPAddress(192,168,1,99), IPAddress(192,168,1,1), IPAddress(255,255,255,0));
                    //wifiManager.autoConnect("IOT-TerZo-INC","test");
                    if (!wifiManager.autoConnect(apName)){
                    Serial.println("failed to connect and hit timeout");
                    delay(3000);
                    //reset and try again, or maybe put it to deep sleep
                    ESP.reset();
                    delay(5000);
                   } 
                  //Sei Visualizza questo messaggio sei connesso
                  Serial.println("connected...yeey :)");

                //leggi aggioramento parametri
                  strcpy(mqtt_server, custom_mqtt_server.getValue());
                  strcpy(mqtt_port, custom_mqtt_port.getValue());
                  strcpy(username, custom_username.getValue());
                  strcpy(password, custom_password.getValue());
                  //salva i paramentri personalizzati in FS
                  if (shouldSaveConfig) {
                    Serial.println("saving config");
                    DynamicJsonBuffer jsonBuffer;
                    JsonObject& json = jsonBuffer.createObject();
                    json["mqtt_server"] = mqtt_server;
                    json["mqtt_port"] = mqtt_port;
                    json["username"] = username;
                    json["password"] = password;
                  
                    File configFile = SPIFFS.open("/config.json", "w");
                    if (!configFile) {
                      Serial.println("failed to open config file for writing");
                    }

                    json.printTo(Serial);
                    json.printTo(configFile);
                    configFile.close();
                    //alla fine salva
                  }
                    Serial.println("local ip");
                    Serial.println(WiFi.localIP());

                   while (WiFi.status() != WL_CONNECTED) {
                    delay(500);
                   Serial.print(".");
               }
                    for (uint8_t t = 2; t > 0; t--) {
                     Serial.printf("[SETUP] WAIT %d...\n", t);
                     Serial.flush();
                     delay(1000);
                     {
                     String mac = getMAC();
    String fwURL = String( fwUrlBase );
    fwURL.concat( mac );
    String fwVersionURL = fwURL;
    fwVersionURL.concat( ".version" );

    Serial.println( "Checking for firmware updates." );
    Serial.print( "MAC address: " );
    Serial.println( mac );
    Serial.print( "Firmware version URL: " );
    Serial.println( fwVersionURL );

    HTTPClient httpClient;
    httpClient.begin( fwVersionURL );
    int httpCode = httpClient.GET();
    if( httpCode == 200 ) {
      String newFWVersion = httpClient.getString();

      Serial.print( "Current firmware version: " );
      Serial.println( FW_VERSION );
      Serial.print( "Available firmware version: " );
      Serial.println( newFWVersion );

      int newVersion = newFWVersion.toInt();

      if( newVersion > FW_VERSION ) {
               Serial.println( "Preparing to update" );

               String fwImageURL = fwURL;
               fwImageURL.concat( ".bin" );
               //ESPhttpUpdate.setLedPin(LED3, LOW);   ---> LED SEGNALAZIONE AGGIORNAMENTO
               t_httpUpdate_return ret = ESPhttpUpdate.update( fwImageURL );

               switch(ret) {
                case HTTP_UPDATE_FAILED:
                Serial.printf("HTTP_UPDATE_FAILD Error (%d): %s", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
                break;

               case HTTP_UPDATE_NO_UPDATES:
               Serial.println("HTTP_UPDATE_NO_UPDATES");
               break;
               }
             }
      else {
        Serial.println( "Already on latest version" );
      }
    }
     else {
      Serial.print( "Firmware version check failed, got HTTP response code " );
      Serial.println( httpCode );
    }
    httpClient.end();
  }

    }
}

//Programma per ricercare nuovi messaggi nel Broker //
/*vecchio call back*/
//void callback(char* topic, byte* payload, unsigned int length) {
void callback(String topic, byte* message, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  // PARTE NUOVA //

  String messageTemp;


  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  
  Serial.println();

  if(topic=="upgrade"){
    Serial.print("preparo per l'upgrade");
        if(messageTemp == "reset"){
           delay(200);
          Serial.print("Avvio Reset");
          ESP.restart(); 
   }
  }
  if(topic=="rele"){
            if(messageTemp == "on"){
          delay(200);
          digitalWrite(RELE,HIGH);
          Serial.print("RELE ATTIVO");
          client.publish(topicpub2, "ON" );
        }
      else if(topic=="rele"){
           if(messageTemp == "off"){
          delay(200);
          digitalWrite(RELE,LOW);
          Serial.print("RELE SPENTO");
          client.publish(topicpub2, "OFF" );
      
      }  
      }
  }
  }
}






// Programma di connessione al Broker  e Sottoscrizione primo messaggio alla connessione //
void reconnect(){
  int count = 0;
   while (!client.connected() && count < 5) {
     count ++;
    Serial.print("MQTT connecting ... ");
    if (client.connect(username, username, password, willTopic, 0, 1, (String("I'm dead: ")+username).c_str())) { // username as client ID
      Serial.println("connected");
      digitalWrite (LED, HIGH);
      String out1 = String(getMAC());
      out1.concat("---> FW = ");
      String out = String ( out1 );
      out.concat(FW_VERSION);
      client.publish(topicpub1, out.c_str() );
      client.subscribe(topic1);
      client.subscribe(topic2);
    } else {
      Serial.print("failed, status code =");
      Serial.print(client.state());
      Serial.println(". riprova tra 5 secondi .");
      digitalWrite (LED, LOW);
      /* Aspetta 5 secondi prima di riprovare */
      delay(5000);
    }
  }
  if (count >= 5 ){
    ESP.restart();
  }
}

//Set-up Seriale , eseguo Routine per rete WiFi //
void setup()
{     
          //pinMode(LED, OUTPUT);
         // pinMode(LED1, OUTPUT);
          //digitalWrite(LED, LOW);
         // digitalWrite(LED1,LOW);
         pinMode(RELE,OUTPUT);
         digitalWrite(RELE, LOW);
          Serial.begin(115200);
          Serial.setDebugOutput(true);   //Se non disponibile Utilizza Serial1 while (!Serial1 ); // wait for serial port to connect D4
          setup_wifi();
          delay(1000);
          dht.setup(14, DHTesp::DHT11); // Inizializzo Sensore
         
                                       // programma di aggiornamento

// Controllo della FingerPrint e settaggio nella rete//

  #ifdef CHECK_FINGERPRINT
    net.setFingerprint(fp);
  #endif
  #if (!defined(CHECK_PUB_KEY) and !defined(CHECK_CA_ROOT) and !defined(CHECK_FINGERPRINT))
    net.setInsecure();
  #endif

// Settaggio dell' indirizzo e porta del Broker //
  client.setServer(mqtt_server, atoi(mqtt_port)); // parseInt to the port
  client.setCallback(callback);

// Lancio sub-routine per eseguire connessione al Broker
  reconnect();
}


//--------------------------------------------- programma aggiornamento //


  void loop() {

  if (!client.connected()) {
    reconnect();
  }client.loop();

   long now = millis();
  if (now - lastMsg > 6000) {
    lastMsg = now;
       
     // misuratore Enel Tensione
     //Raccolta valori dal contattore
          //buffer Statico
      StaticJsonBuffer<300> jsonBuffer;   
      digitalWrite(LED1, HIGH);

    delay(dht.getMinimumSamplingPeriod());

   humidity = dht.getHumidity();
   temperature = dht.getTemperature();

  
     JsonObject& root = jsonBuffer.createObject();
      root["umidita"] = humidity;
      root["temperatura"] = temperature;
      String jsonStr;
      root.printTo(jsonStr);
      root.printTo(Serial);
    
      jsonStr.toCharArray(charBuf, 130);  
      Serial.printf(charBuf);

      client.publish("devices.f5fc70ae-7d25-475e-a5f6-099fd32915c7.events.temperature", charBuf);
      Serial.printf(charBuf);
      digitalWrite(LED1, LOW);
      jsonBuffer = StaticJsonBuffer<300>();   //pulisce Buffer per riscrittura.
   }
        
    delay(6000);
   
  }  
